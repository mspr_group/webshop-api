package fr.epsi.webshopapi.Controller;

import fr.epsi.webshopapi.Model.Customer;
import fr.epsi.webshopapi.Model.Order;
import fr.epsi.webshopapi.Model.Product;
import fr.epsi.webshopapi.Service.CustomerService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;
@RestController
@OpenAPIDefinition(info = @Info(
        title = "Webshop API",
        description = "C'est une API pour le Webshop de l'application PayeTonKawa"))
@RequestMapping("/webshop/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping
    public ResponseEntity<Customer> addCustomer(@Valid @RequestBody Customer customer) {
        Customer result = customerService.addCustomer(customer);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping
    public ResponseEntity<List<Customer>> findAllCustomers() {
        List<Customer> customersList = customerService.getCustomers();
        return ResponseEntity.ok().body(customersList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> findCustomerById(@PathVariable Long id) {
        Customer customer = customerService.getCustomerById(id);
        return ResponseEntity.ok().body(customer);
    }

    @GetMapping("/{customerId}/orders")
    public ResponseEntity<List<Order>> findAllCustomerOrders(@PathVariable Long customerId) {
        return customerService.getOrders(customerId);
    }

    @GetMapping("/{customerId}/orders/{orderId}")
    public ResponseEntity<Order> findOrderById(@PathVariable Long customerId, @PathVariable Long orderId) {
        return customerService.getOrderById(customerId,orderId);
    }

    @GetMapping("/{customerId}/orders/{orderId}/products")
    public ResponseEntity<List<Product>> findProducts(@PathVariable Long customerId, @PathVariable Long orderId) {
        return customerService.getProducts(customerId,orderId);
    }

    @GetMapping("/{customerId}/orders/{orderId}/products/{productId}")
    public ResponseEntity<Product> findProduct(@PathVariable Long customerId, @PathVariable Long orderId, @PathVariable Long productId) {
        return customerService.getProductById(customerId,orderId,productId);
    }

    @PutMapping
    public ResponseEntity<Customer> updateCustomer(@Valid @RequestBody Customer customer) {
        Customer result = customerService.updateCustomer(customer);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable Long id) {
        customerService.deleteCustomer(id);
    }
}