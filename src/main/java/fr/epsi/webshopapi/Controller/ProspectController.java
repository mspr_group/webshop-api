package fr.epsi.webshopapi.Controller;

import java.util.List;

import javax.validation.Valid;

import fr.epsi.webshopapi.Model.Prospect;
import fr.epsi.webshopapi.Service.ProspectService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@OpenAPIDefinition(info = @Info(
        title = "Webshop API",
        description = "C'est une API pour le Webshop de l'application PayeTonKawa"))
@RequestMapping("/webshop/prospects")
public class ProspectController {
    
    @Autowired
    private ProspectService prospectService;

    @PostMapping
    public ResponseEntity<Prospect> addProspect(@Valid @RequestBody Prospect prospect) {
        Prospect result = prospectService.addProspect(prospect);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping
    public ResponseEntity<List<Prospect>> findAllProspects() {
        List<Prospect> prospectsList = prospectService.getProspects();
        return ResponseEntity.ok().body(prospectsList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Prospect> findProspectById(@PathVariable Long id) {
        Prospect prospect = prospectService.getProspectById(id);
        return ResponseEntity.ok().body(prospect);
    }
    
    @PutMapping
    public ResponseEntity<Prospect> updateProspect(@Valid @RequestBody Prospect prospect) {
        Prospect result = prospectService.updateProspect(prospect);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/{id}")
    public void deleteProspect(@PathVariable Long id) {
        prospectService.deleteProspect(id);
    }
}
