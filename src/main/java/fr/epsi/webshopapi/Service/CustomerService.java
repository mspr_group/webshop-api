package fr.epsi.webshopapi.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.epsi.webshopapi.Exception.InvalidRequestException;
import fr.epsi.webshopapi.Exception.NotFoundException;
import fr.epsi.webshopapi.Model.Customer;
import fr.epsi.webshopapi.Model.Order;
import fr.epsi.webshopapi.Model.Product;
import fr.epsi.webshopapi.Repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Value("${resellersAPI.token}")
    private String resellersApiToken;

    public HttpEntity<String> setToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(resellersApiToken);
        return new HttpEntity<>(null, headers);
    }

    public Customer getCustomerById(Long id) { 
        return customerRepository.findById(id).orElseThrow(() -> new NotFoundException("Customer with id "+id+" not found!"));
    }

    public List<Customer> getCustomers() { 
        return customerRepository.findAll();
    }

    public Customer addCustomer(Customer customer) { 
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(Customer customer) {
        if (customer == null || customer.getId() == null) {
            throw new InvalidRequestException("Customer or id must not be null!");
        }
        Customer existingCustomer = getCustomerById(customer.getId());
        existingCustomer.setFirstName(customer.getFirstName());
        existingCustomer.setLastName(customer.getLastName());
        existingCustomer.setAddress(customer.getAddress());
        existingCustomer.setPhoneNumber(customer.getPhoneNumber());
        customerRepository.save(existingCustomer);
        return existingCustomer;
    }

    public void deleteCustomer(Long id) { 
        Customer customer = getCustomerById(id);
        customerRepository.deleteById(customer.getId());
    }
    
    public ResponseEntity<List<Order>> getOrders(Long customerId) {
        getCustomerById(customerId);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange("https://resellersapi-prod.herokuapp.com/resellers/orders?customerId="+customerId, HttpMethod.GET, setToken(), new ParameterizedTypeReference<List<Order>>() {});
    }

    public ResponseEntity<Order> getOrderById(Long customerId, Long orderId) {
        getCustomerById(customerId);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange("https://resellersapi-prod.herokuapp.com/resellers/orders/"+customerId+"/"+orderId, HttpMethod.GET, setToken(), Order.class);
    };

    public ResponseEntity<List<Product>> getProducts(Long customerId, Long orderId) {
        if (getOrderById(customerId, orderId) == null || !getOrderById(customerId, orderId).hasBody()) {
            throw new NullPointerException("Order with id " + orderId + " not found!");
        }
        Order order = getOrderById(customerId, orderId).getBody();
        if (order == null) {
            throw new NullPointerException("Order with id " + orderId + " not found!");
        }
        return ResponseEntity.ok().body(order.getProducts());
    };

    public ResponseEntity<Product> getProductById(Long customerId, Long orderId, Long productId) {
        getCustomerById(customerId);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange("https://resellersapi-prod.herokuapp.com/resellers/products/"+customerId+"/"+orderId+"/"+productId, HttpMethod.GET, setToken(), Product.class);
    }

    
}
