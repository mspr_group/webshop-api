package fr.epsi.webshopapi.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name="Person")
public class Person {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "Firstname can not be null")
    @NotBlank(message = "Firstname can not be empty")
    @Size(min = 2, max = 14, message = "Firstname must be between 2 and 14 characters")
    private String firstName;
    @NotNull(message = "Lastname can not be null")
    @NotBlank(message = "Lastname can not be empty")
    @Size(min = 2, max = 14, message = "Lastname must be between 2 and 14 characters")
    private String lastName;
    @ManyToOne(cascade=CascadeType.ALL)
    private Address address;
    @NotNull(message = "Phone number can not be null")
    @NotBlank(message = "Phone number can not be empty")
    @Size(min = 10, max = 14, message = "Phone number must be between 10 and 14 characters")
    private String phoneNumber;

    public Person() {

    }

    public Person(Long id, String firstName, String lastName, Address address, String phoneNumber) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public Person(String firstName, String lastName, Address address, String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }
}
