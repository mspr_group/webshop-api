package fr.epsi.webshopapi.Model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Prospect")
public class Prospect extends Person{
    public Prospect() {
        super();
    }

    public Prospect(Long id, String firstName, String lastName, Address address, String phoneNumber) {
        super(id, firstName, lastName, address, phoneNumber);
    }

    public Prospect(String firstName, String lastName, Address address, String phoneNumber) {
        super(firstName, lastName, address, phoneNumber);
    }
}
