package fr.epsi.webshopapi.Repository;

import fr.epsi.webshopapi.Model.Customer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    
}
