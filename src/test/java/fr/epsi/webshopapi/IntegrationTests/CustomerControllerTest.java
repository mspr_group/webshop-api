package fr.epsi.webshopapi.IntegrationTests;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import fr.epsi.webshopapi.Model.Address;
import fr.epsi.webshopapi.Model.Customer;
import fr.epsi.webshopapi.Repository.CustomerRepository;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;

@AutoConfigureMockMvc(addFilters = false)
@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
class CustomerControllerTest {
    
    @Autowired
    private MockMvc mvc;
 
    @Autowired
    private CustomerRepository customerRepository;

    @Container
    public static PostgreSQLContainer<?> container = new PostgreSQLContainer<>("postgres:11.1")
        .withDatabaseName("webshop")
        .withUsername("postgres")
        .withPassword("pass");

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.password", container::getPassword);
        registry.add("spring.datasource.username", container::getUsername);
    }

    @BeforeAll
    static void initAll() {
        container.start();
    }

    @Test
    @Order(1)
    public void testAddCustomer() throws Exception {
        Customer customer = new Customer();
        customer.setFirstName("Youness");
        customer.setLastName("ELGHAZI");
        customer.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
        customer.setPhoneNumber("0606060606");

        customerRepository.save(customer);

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(customer);

        mvc.perform(post("/webshop/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(requestJson))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()));
    }

    @Test
    @Order(2)
    public void testGetAllCustomers() throws Exception {
        mvc.perform(get("/webshop/customers")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
            .andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].firstName", is("Youness")));
    }

    @Test
    @Order(3)
    public void testGetCustomerById() throws Exception {
        Customer customer = new Customer();
        customer.setFirstName("Youcef");
        customer.setLastName("MERZOUGUI");
        customer.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
        customer.setPhoneNumber("0606060505");

        Long customerId = customerRepository.save(customer).getId();

        mvc.perform(get("/webshop/customers/"+customerId)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
			.andExpect(jsonPath("$.firstName", is("Youcef")));
    }

    @Test
    @Order(4)
    public void testUpdateCustomer() throws Exception {
        Customer customer = new Customer();
        customer.setFirstName("Ismail");
        customer.setLastName("Rekis");
        customer.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
        customer.setPhoneNumber("0753189114");

        Long customerId = customerRepository.save(customer).getId();

        customer.setId(customerId);
        customer.setAddress(new Address("5bis", "Boulevard de Berlin", "Nantes", "44000"));
        
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(customer);

        mvc.perform(put("/webshop/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(requestJson))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
			.andExpect(jsonPath("$.address.street", is("Boulevard de Berlin")))
            .andExpect(jsonPath("$.address.city", is("Nantes")))
            .andExpect(jsonPath("$.address.postalCode", is("44000")));
    }

    @Test
    @Order(5)
    public void testDeleteCustomer() throws Exception {
        Customer customer = new Customer();
        customer.setFirstName("Ryan");
        customer.setLastName("LE BRUN");
        customer.setAddress(new Address("5bis", "Rue René Prolongée", "Marseille", "44000"));
        customer.setPhoneNumber("0753189114");

        Long customerId = customerRepository.save(customer).getId();

        mvc.perform(delete("/webshop/customers/"+customerId)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

}
