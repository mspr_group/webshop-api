package fr.epsi.webshopapi.UnitTests;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.epsi.webshopapi.Controller.CustomerController;
import fr.epsi.webshopapi.Model.Address;
import fr.epsi.webshopapi.Model.Customer;
import fr.epsi.webshopapi.Model.Order;
import fr.epsi.webshopapi.Model.Product;
import fr.epsi.webshopapi.Service.CustomerService;

@WebMvcTest(controllers = CustomerController.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc(addFilters = false)
class CustomerControllerTests {

	@Autowired
    private MockMvc mockMvc;

	@Autowired
    ObjectMapper mapper;

	@MockBean
    private RestTemplate restTemplate;

    @MockBean
    private CustomerService customerService;
	
	Customer CUSTOMER_1 = new Customer(1L, "Youness", "ELGHAZI", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0615462157");
	Customer CUSTOMER_2 = new Customer(2L, "Youcef", "MERZOUGUI", new Address("10", "Rue de Berlin", "Lyon", "69003"), "0516485120");
	Customer CUSTOMER_3 = new Customer(3L, "Ismail", "REKIS", new Address("22ter", "Rue René Prolongée", "Lyon", "69003"), "0617423958");
	Customer CUSTOMER_4 = new Customer(4L, "Ryan", "LE BRUN", new Address("2", "Rue René Prolongée", "Lyon", "69003"), "0617495231");

	Product PRODUCT_1 = Product.builder().id(1L).name("Renard Herve").libelle("machine à cafe").price("99.99").description("machine à café intelligente").color("noir").stock(10).build();
	Product PRODUCT_2 = Product.builder().id(2L).name("Renard Herve").libelle("tasse de café").price("19.99").description("tasse en bois").color("noir").stock(20).build();

	Order ORDER_1 = Order.builder().id(1L).customerId(1L).products(new ArrayList<>(Arrays.asList(PRODUCT_1, PRODUCT_2))).build();
	Order ORDER_2 = Order.builder().id(1L).customerId(1L).products(new ArrayList<>(Arrays.asList(PRODUCT_1))).build();
	
	@Test
    public void testGetCustomers() throws Exception {
		List<Customer> customerList = new ArrayList<>(Arrays.asList(CUSTOMER_1, CUSTOMER_2, CUSTOMER_3, CUSTOMER_4));
		Mockito.when(customerService.getCustomers()).thenReturn(customerList);
        mockMvc.perform(MockMvcRequestBuilders
			.get("/webshop/customers")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(4)))
			.andExpect(jsonPath("$[2].firstName", is("Ismail")))
			.andDo(MockMvcResultHandlers.print());
    }

	@Test
    public void testGetCustomerById() throws Exception {
		Mockito.when(customerService.getCustomerById(CUSTOMER_1.getId())).thenReturn(CUSTOMER_1);
        mockMvc.perform(MockMvcRequestBuilders
			.get("/webshop/customers/"+1L)
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", notNullValue()))
			.andExpect(jsonPath("$.firstName", is("Youness")));
    }

	@Test
	public void testAddCustomer() throws Exception {

    	Mockito.when(customerService.addCustomer(CUSTOMER_1)).thenReturn(CUSTOMER_1);
		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/webshop/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(CUSTOMER_1));

		mockMvc.perform(mockRequest)
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
            .andExpect(jsonPath("$.firstName", is("Youness")));
	}

	@Test
	public void testUpdateCustomer() throws Exception {
		Customer updatedRecord = new Customer();
		updatedRecord.setId(1L);
		updatedRecord.setFirstName("Ahmed");
		updatedRecord.setLastName("ELGHAZI");
		updatedRecord.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
		updatedRecord.setPhoneNumber("0751189114");

    	Mockito.when(customerService.updateCustomer(updatedRecord)).thenReturn(updatedRecord);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/webshop/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(updatedRecord));

		mockMvc.perform(mockRequest)
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
            .andExpect(jsonPath("$.firstName", is("Ahmed")))
			.andExpect(jsonPath("$.phoneNumber", is("0751189114")));
	}

	@Test
	public void testDeleteCustomer() throws Exception {
    	Mockito.when(customerService.getCustomerById(2L)).thenReturn(CUSTOMER_2);

    	mockMvc.perform(MockMvcRequestBuilders
            .delete("/webshop/customers/"+2L)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
	}

	@Test
	public void testGetOrders() throws Exception {
		List<Order> ordersList = new ArrayList<Order>(Arrays.asList(ORDER_1,ORDER_2));
		ResponseEntity<List<Order>> ResEntOrdersList = ResponseEntity.ok().body(ordersList);
		Mockito.when(customerService.getOrders(1L)).thenReturn(ResEntOrdersList);
        mockMvc.perform(MockMvcRequestBuilders
			.get("/webshop/customers/"+1L+"/orders")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(2)))
			.andExpect(jsonPath("$[1].customerId", is(1)));
	}

	@Test
	public void testGetOrderByCustomerId() throws Exception {
		ResponseEntity<Order> order = ResponseEntity.ok().body(ORDER_1);
		Mockito.when(customerService.getOrderById(1L, 1L)).thenReturn(order);
        mockMvc.perform(MockMvcRequestBuilders
			.get("/webshop/customers/"+1L+"/orders/"+1L)
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", notNullValue()))
			.andExpect(jsonPath("$.customerId", is(1)))
			.andExpect(jsonPath("$.id", is(1)));
	}

	@Test
	public void testGetProducts() throws Exception {
		List<Product> productsList = new ArrayList<Product>(Arrays.asList(PRODUCT_1,PRODUCT_2));
		ResponseEntity<List<Product>> ResEntProductsList = ResponseEntity.ok().body(productsList);
		Mockito.when(customerService.getProducts(1L, 1L)).thenReturn(ResEntProductsList);
        mockMvc.perform(MockMvcRequestBuilders
			.get("/webshop/customers/"+1L+"/orders/"+1L+"/products")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(2)))
			.andExpect(jsonPath("$[0].libelle", is("machine à cafe")));
	}

	@Test
	public void testGetProductById() throws Exception {
		ResponseEntity<Product> product = ResponseEntity.ok().body(PRODUCT_2);
		Mockito.when(customerService.getProductById(1L, 1L, 2L)).thenReturn(product);
        mockMvc.perform(MockMvcRequestBuilders
			.get("/webshop/customers/"+1L+"/orders/"+1L+"/products/"+2L)
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", notNullValue()))
			.andExpect(jsonPath("$.libelle", is("tasse de café")));
	}
}
