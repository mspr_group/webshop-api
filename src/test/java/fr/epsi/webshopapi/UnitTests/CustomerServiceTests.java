package fr.epsi.webshopapi.UnitTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException.NotFound;

import fr.epsi.webshopapi.Exception.InvalidRequestException;
import fr.epsi.webshopapi.Exception.NotFoundException;
import fr.epsi.webshopapi.Model.Address;
import fr.epsi.webshopapi.Model.Customer;
import fr.epsi.webshopapi.Model.Order;
import fr.epsi.webshopapi.Model.Product;
import fr.epsi.webshopapi.Repository.CustomerRepository;
import fr.epsi.webshopapi.Service.CustomerService;

@ExtendWith(MockitoExtension.class)
@RestClientTest(CustomerService.class)
@AutoConfigureWebClient(registerRestTemplate=true)
@ContextConfiguration(classes = CustomerRepository.class)
public class CustomerServiceTests {

    @Spy
    @InjectMocks
	CustomerService customerService;

	@Mock
	CustomerRepository customerRepository;

    @Mock
    private RestTemplate restTemplate;

    private String resellersApiToken = System.getProperty("token");

    Customer CUSTOMER_1 = new Customer(1L, "Youness", "ELGHAZI", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0615462157");
	Customer CUSTOMER_2 = new Customer(2L, "Youcef", "MERZOUGUI", new Address("10", "Rue de Berlin", "Lyon", "69003"), "0516485120");
	
    Product PRODUCT_1 = Product.builder().id(1L).name("Renard Herve").libelle("machine à cafe").price("99.99").description("machine à café intelligente").color("noir").stock(10).build();
	Product PRODUCT_2 = Product.builder().id(2L).name("Renard Herve").libelle("tasse de café").price("19.99").description("tasse en bois").color("noir").stock(20).build();

	Order ORDER_1 = Order.builder().id(1L).customerId(1L).products(new ArrayList<>(Arrays.asList(PRODUCT_1, PRODUCT_2))).build();
	Order ORDER_2 = Order.builder().id(1L).customerId(1L).products(new ArrayList<>(Arrays.asList(PRODUCT_1))).build();
    
    @Test
	public void testGetCustomers()
	{
		List<Customer> customerList = new ArrayList<>(Arrays.asList(CUSTOMER_1, CUSTOMER_2));

		Mockito.when(customerRepository.findAll()).thenReturn(customerList);

		//test
		List<Customer> result = customerService.getCustomers();

		assertEquals(2, result.size());
		verify(customerRepository, times(1)).findAll();
	}

    @Test
	public void testGetCustomerById()
	{
		Mockito.when(customerRepository.findById(2L)).thenReturn(Optional.of(CUSTOMER_2));

		//test
		Customer result = customerService.getCustomerById(2L);

		assertEquals(2L, result.getId());
		assertEquals("Youcef", result.getFirstName());
		assertEquals("MERZOUGUI", result.getLastName());
        assertEquals("Rue de Berlin", result.getAddress().getStreet());
        assertEquals("0516485120", result.getPhoneNumber());
	}

    @Test
    public void testGetCustomerByIdNotFoundException() throws Exception {

        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> customerService.getCustomerById(5L),
           "Customer with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Customer with id 5 not found!"));
    }

    @Test
	public void testAddCustomer() {

		Mockito.when(customerRepository.save(CUSTOMER_1)).thenReturn(CUSTOMER_1);

		//test
		Customer result = customerService.addCustomer(CUSTOMER_1);

		assertEquals("Youness", result.getFirstName());
		assertEquals("ELGHAZI", result.getLastName());
        assertEquals("5bis", result.getAddress().getNumber());
        assertEquals("Rue René Prolongée", result.getAddress().getStreet());
        assertEquals("Lyon", result.getAddress().getCity());
        assertEquals("69003", result.getAddress().getPostalCode());
        assertEquals("0615462157", result.getPhoneNumber());
	}

    @Test
	public void testUpdateCustomer() {
        Customer customer = new Customer(1L, "Ahmed", "DIAGNE", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0618923456");
        
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.ofNullable(customer));
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);

		//test
		Customer result = customerService.updateCustomer(customer);

		assertEquals("Ahmed", result.getFirstName());
		assertEquals("DIAGNE", result.getLastName());
        assertEquals("5bis", result.getAddress().getNumber());
        assertEquals("Rue René Prolongée", result.getAddress().getStreet());
        assertEquals("Lyon", result.getAddress().getCity());
        assertEquals("69003", result.getAddress().getPostalCode());
        assertEquals("0618923456", result.getPhoneNumber());
	}

    @Test
    public void testUpdateCustomerNotFoundException() throws Exception {
        Customer customer = new Customer(5L, "Ahmed", "DIAGNE", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0618923456");

        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> customerService.updateCustomer(customer),
           "Customer with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Customer with id 5 not found!"));
    }

    @Test
    public void testUpdateCustomerWithNullId() throws Exception {
        Customer customer = new Customer(null, "Ahmed", "DIAGNE", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0618923456");

        InvalidRequestException thrown = assertThrows(
            InvalidRequestException.class,
           () -> customerService.updateCustomer(customer),
           "Customer or id must not be null!"
        );

        assertTrue(thrown.getMessage().contentEquals("Customer or id must not be null!"));
    }

    @Test
    public void testUpdateCustomerWithNullCustomer() throws Exception {
        InvalidRequestException thrown = assertThrows(
            InvalidRequestException.class,
           () -> customerService.updateCustomer(null),
           "Customer or id must not be null!"
        );

        assertTrue(thrown.getMessage().contentEquals("Customer or id must not be null!"));
    }

    @Test
    public void testDeleteCustomer() throws Exception {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));

        customerService.deleteCustomer(1L);

        // verify the mocks
        verify(customerRepository, times(1)).deleteById(1L);

    }

    @Test
    public void testDeleteCustomerNotFound() throws Exception {
        Mockito.when(customerRepository.findById(5L)).thenThrow(new NotFoundException("Customer with id 5 not found!"));
        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> customerService.deleteCustomer(5L),
           "Customer with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Customer with id 5 not found!"));
    }

    @Test
    public void testGetOrders() throws Exception {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.ofNullable(CUSTOMER_1));

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(resellersApiToken);

        Mockito.when(customerService.setToken()).thenReturn(new HttpEntity<>(null, headers));

        ResponseEntity<List<Order>> result = customerService.getOrders(1L);

        assertNotNull(result.getBody());
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void testGetOrdersCustomerNotFound() throws Exception {
        Mockito.when(customerRepository.findById(5L)).thenThrow(new NotFoundException("Customer with id 5 not found!"));

        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> customerService.getOrders(5L),
           "Customer with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Customer with id 5 not found!"));
    }

    @Test
    public void testGetOrderById() throws Exception {   
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(resellersApiToken);

        Mockito.when(customerService.setToken()).thenReturn(new HttpEntity<>(null, headers));

        ResponseEntity<Order> result = customerService.getOrderById(1L,6L);

        assertEquals(200, result.getStatusCodeValue());
        assertNotNull(result.getBody());
        assertEquals(1L, result.getBody().getCustomerId());
        assertEquals(6L, result.getBody().getId());
    }

    @Test
    public void testGetOrderByIdCustomerNotFound() throws Exception {
        Mockito.when(customerRepository.findById(5L)).thenThrow(new NotFoundException("Customer with id 5 not found!"));
        
        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> customerService.getOrderById(5L,1L),
           "Customer with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Customer with id 5 not found!"));
    }

    @Test
    public void testGetOrderByIdOrderNotFound() throws Exception {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(resellersApiToken);

        Mockito.when(customerService.setToken()).thenReturn(new HttpEntity<>(null, headers));
        
        NotFound thrown = assertThrows(
            NotFound.class,
           () -> customerService.getOrderById(1L,5L),
           "Order with id 5 not found!"
        );
        assertEquals(404, thrown.getStatusCode().value());
    }

    @Test
    public void testGetProducts() throws Exception {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(resellersApiToken);
        Mockito.when(customerService.setToken()).thenReturn(new HttpEntity<>(null, headers));

        ResponseEntity<List<Product>> result = customerService.getProducts(1L,6L);

        assertTrue(result.getBody().size()>0);
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void testGetProductsCustomerNotFound() throws Exception {
        Mockito.when(customerRepository.findById(5L)).thenThrow(new NotFoundException("Customer with id 5 not found!"));

        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> customerService.getProducts(5L,1L),
           "Customer with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Customer with id 5 not found!"));
    }

    @Test
    public void testGetProductsOrderNotFound() throws Exception {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(resellersApiToken);
        Mockito.when(customerService.setToken()).thenReturn(new HttpEntity<>(null, headers));

        NotFound thrown = assertThrows(
            NotFound.class,
           () -> customerService.getProducts(1L,5L),
           "Order with id 5 not found!"
        );

        assertEquals(404, thrown.getStatusCode().value());
    }

    @Test
    public void testGetProductById() throws Exception {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(resellersApiToken);
        Mockito.when(customerService.setToken()).thenReturn(new HttpEntity<>(null, headers));

        ResponseEntity<Product> result = customerService.getProductById(1L,6L,3L);

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(3L, result.getBody().getId());
        assertNotNull(result.getBody());
    }

    @Test
    public void testGetProductByIdCustomerNotFound() throws Exception {
        Mockito.when(customerRepository.findById(5L)).thenThrow(new NotFoundException("Customer with id 5 not found!"));

        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> customerService.getProductById(5L,1L,1L),
           "Customer with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Customer with id 5 not found!"));
    }

    @Test
    public void testGetProductByIdOrderNotFound() throws Exception {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(resellersApiToken);
        Mockito.when(customerService.setToken()).thenReturn(new HttpEntity<>(null, headers));

        NotFound thrown = assertThrows(
            NotFound.class,
           () -> customerService.getProductById(1L,5L,1L),
           "Order with id 5 not found!"
        );

        assertEquals(404, thrown.getStatusCode().value());
    }

    @Test
    public void testGetProductByIdProductNotFound() throws Exception {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(resellersApiToken);
        Mockito.when(customerService.setToken()).thenReturn(new HttpEntity<>(null, headers));

        NotFound thrown = assertThrows(
            NotFound.class,
           () -> customerService.getProductById(1L,6L,1L),
           "Product with id 5 not found!"
        );

        assertEquals(404, thrown.getStatusCode().value());
    }
}
